$.extend($.fancybox.defaults, {
    hash: false,
    //infobar: true,
    buttons: [
        'close'
    ],
    margin: [80, 28],
    lang: 'ru',
    transitionEffect: "slide",
    idleTime : 0,
    loop : true,
    clickContent: false,
    zoomOpacity : true,
    touch : {
    vertical : false,  // Allow to drag content vertically
    momentum : false   // Continuous movement when panning
},
    i18n: {
        'ru': {
            CLOSE: 'Закрыть',
            NEXT: 'Далее',
            PREV: 'Назад',
            ERROR: 'Не удалось открыть галерею - попробуйте позже',
            PLAY_START: 'Запустить слайдшоу',
            PLAY_STOP: 'Пауза',
            FULL_SCREEN: 'На весь экран',
            THUMBS: 'Миниатуры'
        },
    /*caption : function( instance, item ) {
        var caption = $(this).attr('title') || '';

        return caption;
    }*/
    },
    //afterClose: function() {}
});